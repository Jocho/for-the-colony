<?php
namespace Clair;

const ROLE_ADMIN     = 1;
const ROLE_MODERATOR = 2;
const ROLE_USER      = 3;
const AT_LENGTH      = 32;
const VALID_TO_MOD   = 2678400;

class Users extends Middleware {
	private $salt   = 'packmyboxwithfivedozenliquorjugs_0123456789';

	public function get() {
		if ($this->checkId()) {
			$user = $this->getById($this->app->request->vars['id']);
			$this->app->request->input['email'] = $user->email;

			if ($this->isLogged())
				$this->controller->template->user = $user;
			else
				$this->controller->template->errors[] = 'You have no rights to view this user data.';
		}
	}
	
	public function put() {
		$in   = $this->app->request->input;
		$user = new \Clair\Object;


		if (!isset($in['action'])) {
			$this->controller->template->errors[] = 'No action specified.';
			return;
		}

		switch ($in['action']) {
			case 'at_login':
				if ($this->checkAccessToken())
					$user = $this->getByToken($in['access_token']);
				break;

			case 'register':
				$this->registerUser();
				break;

			case 'login':
				if ($this->checkEmail() && $this->checkPassword()) {
					$user = $this->getUser($in);
				}
				break;

			case 'logout':
				$this->app->session->del('user');
				$this->controller->template->user = new \Clair\Object;
				setcookie('user_token', NULL, time() -1, '/');
				break;

			case 'update':
				if ($this->isLogged()
					&& $this->checkEmail() 
					&& $this->checkPassword() 
					&& $this->checkAccessToken() 
				) {
					$user = $this->updateUser($in);
				}
				break;

			default: 
				$this->controller->template->errors[] = 'Unknown action called.';
				break;
		}

		if (!$user->isEmpty()) {
			$this->sessionUser($user);
			$this->controller->template->user = clone $user;
			$this->controller->template->user->del('id');
			$this->controller->template->user->del('password');

			if (!$this->app->request->isAjax)
				$this->app->redirect();
		}
		else {
			$this->controller->template->errors[] = 'User could not be loaded.';
		}
	}

	public function post() {
		if ($this->app->request->isAjax) {
			$this->registerUser();
			$this->app->redirect('{admin}');
		}
		else
			$this->put();
	}

	public function delete() {
		if (!$this->checkAccessToken() || !$this->isLogged())
			return;

		$user = $this->app->session->get('user');

		$this->app->database
			->table('users')
			->delete()
			->where('id', $user->id);

		$this->app->database
			->table('access_tokens')
			->delete()
			->where('user_id', $user->id);

		$this->app->database
			->table('characters')
			->delete()
			->where('user_id', $user->id);

		$this->app->database
			->table('adventures')
			->delete()
			->where('user_id', $user->id);
	}

	public function isLogged() {
		return $this->app->session->has('user') 
			&& $this->app->session->user->has('email') 
			&& (!isset($this->app->request->input['email']) || $this->app->session->user->email == $this->app->request->input['email'])
			&& $this->checkAccessToken();
	}

	public function createUser($in) {
		$id = $this->app->database
			->table('users')
			->insert(array(
				'email'        => $in['email'],
				'password'     => $this->getHash($in['password'], $in['email']),
				'cdate'        => time(),
				'mdate'        => time(),
				'role_id'      => ROLE_USER
			));

		return $this->getById($id);
	}

	private function registerUser() {
		$user = ($this->checkEmail() 
			&& !$this->userExists($this->app->request->input['email'])
			&& $this->checkPassword() 
			&& $this->passwordsMatch()
		)
			? $this->createUser($this->app->request->input)
			: new \Clair\Object;

		if (!$user->isEmpty()) {
			$user->set('access_token', $this->setAccessToken($user));
			$this->sessionUser($user);
			$this->controller->template->user = clone $user;
			$this->controller->template->user->del('id');
			$this->controller->template->user->del('password');

			if (!$this->app->request->isAjax)
				$this->app->redirect();
		}
		else {
			$this->controller->template->errors[] = 'User could not be loaded.';
		}
	}

	private function sessionUser($user) {
		$this->app->session->set('user', $user);
		setcookie('user_token', $user->access_token, time() + VALID_TO_MOD, '/');
	}

	private function getUser($input = array()) {
		$user = new \Clair\Object($this->app->database
			->table('users')
			->select('users.*', 'at.access_token AS access_token')
			->leftJoin('access_tokens AS at', 'users.id', '=', 'at.user_id')
			->where(array(
				'email'    => $input['email'],
				'password' => $this->getHash($input['password'], $input['email'])
			))
			->first());

		if (!$user->has('access_token'))
			$user->set('access_token', $this->setAccessToken($user));

		return new \Clair\Object($user);
	}

	private function updateUser($in = array()) {
		$this->app->database
			->table('users')
			->join('access_tokens AS at', 'users.id', '=', 'at.users_id')
			->where('at.access_token', $in['access_token'])
			->update(array(
				'email'    => $in['email'],
				'password' => $this->getHash($in['password'], $in['email']),
				'mdate'    => time()
			));

		return $this->getByEmail($in['email']);
	}

	private function setAccessToken($user) {
		$at = $this->app->database
			->table('access_tokens')
			->where('user_id', $user->id)
			->first();

		$token = $this->randomString(AT_LENGTH);

		if ($at) {
			$this->app->database
				->table('access_tokens')
				->where('user_id', $user->id)
				->update(array(
					'access_token' => $token,
					'valid_to'     => time() + VALID_TO_MOD
				));
		}
		else {
			$this->app->database
				->table('access_tokens')
				->insert(array(
					'user_id'      => $user->id,
					'access_token' => $token,
					'valid_to'     => time() + VALID_TO_MOD
				));
		}

		return $token;
	}

	private function getById($id = 0) {
		return new \Clair\Object($this->app->database
			->table('users')
			->where('id', $id)
			->first());
	}

	private function getByEmail($email = '') {
		return new \Clair\Object($this->app->database
			->table('users')
			->where('email', $search)
			->first());
	}

	private function getByToken($token = '') {
		return new \Clair\Object($this->app->database
			->table('users')
			->select('users.*', 'at.access_token AS access_token')
			->join('access_tokens AS at', 'users.id', '=', 'at.user_id')
			->where(array(
				'at.access_token' => $token,
				'valid_to >='     => time()
			))
			->first());
	}

	private function userExists($search = '', $searchIn = 'email') {
		$x = $this->app->database
			->table('users')
			->where($searchIn . ' %%', $search)
			->first();

		return !empty($x);
	}

	public function getHash($pass = '', $salt = '') {
		return md5($pass . $this->salt . $salt);
	}

	public function randomString($length = 12) {
		$t = $this->salt;
		$s = '';
		for ($i = 0; $i < $length; $i++) {
			$l  = substr($t, rand(0, 36), 1);
			$s .= rand(0, 1) ? strtoupper($l) : $l;
		}	
		return $s;
	}

	private function checkId() {
		if (strpos($this->app->request->path, $this->app->settings->redirections->get('users', '/users')) === FALSE)
			return FALSE;

		if (!isset($this->app->request->vars['id'])) {
			$this->controller->template->errors[] = 'No ID was sent.';
			return FALSE;
		}
		return TRUE;
	}

	private function checkEmail() {
		if (!isset($this->app->request->input['email'])) {
			$this->controller->template->errors[] = 'No e-mail was sent.';
			return FALSE;
		}

		$input = $this->app->request->input;
		
		if (!preg_match('/^[\w\.]+@[\w\d\.-]+\.\w{2,}$/', $input['email'])) {
			$this->controller->template->errors[] = 'E-mail is not valid.';
			return FALSE;
		}

		return TRUE;
	}

	private function checkAccessToken() {
		if (!isset($this->app->request->input['access_token'])) {
			$this->controller->template->errors[] = 'No access token was sent.';

			return strlen($this->app->session->user->get('access_token', '')) > 0;
		}

		if (strlen($this->app->request->input['access_token']) != AT_LENGTH) {
			$this->controller->template->errors[] = 'Set access token is invalid.';
			return FALSE;
		}

		if (!$this->getByToken($this->app->request->input['access_token'])) {
			$this->controller->template->errors[] = 'Set access token timed out.';
			return FALSE;
		}

		return TRUE;
	}

	private function checkPassword() {
		if (!isset($this->app->request->input['password'])) {
			$this->controller->template->errors[] = 'No password was sent.';
			return FALSE;
		}

		if (!strlen($this->app->request->input['password'])) {
			$this->controller->template->errors[] = 'Password is not valid.';
			return FALSE;
		}
		return TRUE;
	}

	private function passwordsMatch() {
		if (!isset($this->app->request->input['password2']) || $this->app->request->input['password'] != $this->app->request->input['password2']) {
			$this->controller->template->errors[] = 'Passwords do not match.';
			return FALSE;
		}
		return TRUE;
	}
}