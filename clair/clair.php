<?php
namespace Clair;

class Clair {
	private $loggedMessages = 0;
	private $controller;
	private $view;
	
	public $isDevMode;
	public $settings;
	public $database;
	public $session;
	public $strings;
	public $request;
	
	public function __construct() {
		require_once('strings.php');
		require_once('database.php');

		$this->request  = new \Clair\Request;
		$this->settings = new \Clair\Object;
		$this->strings  = new \Clair\Strings;
		$this->session  = new \Clair\Session;
		$this->setPaths();
		$this->loadSettings();

		$this->database = new \Clair\Database($this);

		$this->setController();
		$this->run();
	}

	private function loadSettings() {
		$wlFile          = FRAMEWORK . 'whitelist.txt';
		$whitelist       = (file_exists($wlFile)) ? file($wlFile) : [];
		$this->isDevMode = FALSE;
		$ip              = $_SERVER["SERVER_ADDR"];

		foreach ($whitelist as $addr) {
			if (in_array($addr, $whitelist)) {
				$this->isDevMode = TRUE;
				break;
			}
		}

		$settingsFile  = FRAMEWORK;
		$settingsFile .= ($this->isDevMode) ? 'settings-dev.json' : 'settings-prod.json';

		try {
			if (!file_exists($settingsFile))
				throw new \Exception('The settings file "'. $settingsFile . '" does not exist.');

			$this->settings->fromJSON(file_get_contents($settingsFile));
		}
		catch(\Exception $e) {
			print_r($e->getMessage());
			die();
		}
	}

	private function setPaths() {
		define('ROOT', str_replace('\\', '/', dirname(dirname(__FILE__)) . '\\'));
		define('ASSETS', ROOT . 'assets/');
		define('FRAMEWORK', ROOT . 'clair/');
		define('CONTROLLERS', ROOT . 'controllers/');
		define('DATABASE', ROOT . 'db/');
		define('JAVASCRIPT', ROOT . 'js/');
		define('MIDDLEWARE', ROOT . 'middleware/');
		define('RESOURCES', ROOT . 'resources/');
		define('VIEWS', ROOT . 'views/');
	}

	private function setController() {
		$routeFitnesses   = [];
		$routeVars        = [];
		$routeControllers = [];
		$mainRoute        = $this->request->path;
		
		foreach ($this->settings->routes->toArray() as $controller => $routes) {
			$routes = (is_array($routes)) ? $routes : array($routes);

			foreach ($routes as $key => $route) {
				$routeControllers[$route] = $controller;
				list($routeFitnesses[$route], $routeVars[$route]) = $this->checkRouteFitness($mainRoute, $route);
			}
		}

		asort($routeFitnesses);
		$routeFitnesses = array_reverse($routeFitnesses);

		if (reset($routeFitnesses)) {
			$rfitt = array_keys($routeFitnesses);
			$route = reset($rfitt);

			$this->request->vars = $routeVars[$route];
			$controllerName      = $routeControllers[$route];
			$controllerFile      = substr(strtolower($this->strings->changeSlashes($controllerName)), 1);
			$controllerPath      = CONTROLLERS . $controllerFile . '.php';
			$this->controller    = $this->loadAndInit($controllerPath, $controllerName);
		}
		else {
			$this->redirect($this->settings->redirections->e404);
		}
	}

	public function loadAndInit($controllerPath, $controllerName, &$object = NULL) {
		$objectType = (is_null($object)) ? 'Controller' : 'Middleware';
		$appRef     = (isset($this->app)) ? $this->app : $this;
		try {
			if (!file_exists($controllerPath))
				throw (new \Exception($objectType . ' file "' . $controllerPath . '" does not exist.'));

			require_once($controllerPath);

			try {
				if (!class_exists($controllerName))
					throw (new \Exception($objectType . ' class "' . $controllerName . '" does not exist.'));
				return new $controllerName($appRef, $object);
			}
			catch (\Exception $e) {
				print_r($e->getMessage());
				die();
			}

		}
		catch (\Exception $e) {
			print_r($e->getMessage());
			die();
		}
	}

	private function run() {
		$vendorAutoload = ROOT . 'vendor/autoload.php';
		if (file_exists($vendorAutoload))
			require_once($vendorAutoload);

		$this->processRun($this->controller);
		$this->session->save();

		if ($this->loggedMessages)
			$this->log("Logged: ". $this->loggedMessages . " message(s).\n");
	}

	public function processRun($controller) {
		foreach ($controller->getMiddleware('before') as $middleware) {
			if (method_exists($middleware, $controller->app->request->method))
				$middleware->{$controller->app->request->method}($controller);
		}

		$controller->{$controller->app->request->method}();

		foreach ($controller->getMiddleware('after') as $middleware) {
			if (method_exists($middleware, $controller->app->request->method))
				$middleware->{$controller->app->request->method}($controller);
		}

		new \Clair\View($controller);
	}

	private function checkRouteFitness($mainRoute, $route) {
		$fitness   = 0;
		$routeVars = [];
		$r1        = explode('/', $mainRoute);
		$r2        = explode('/', $route);

		foreach ($r1 as $k => $level) {
			if (!isset($r2[$k]))
				break;

			if (!strcmp($level, $r2[$k]))
				$fitness += pow(10, $k + 1);
			elseif (strlen($level) && preg_match('/\{(\w+)\}/', $r2[$k], $match) && isset($match[1])) {
				$routeVars[$match[1]] = $level;
				$fitness += pow(10, $k + 1) - pow(10, $k) / 2;
			}

			$fitness += count($r1) - count($r2);
		}
		return array($fitness, $routeVars);
	}

	public function redirect($path = NULL) {
		$this->request->input = [];
		$this->session->save();
		$redirectTo = (is_string($path) && strlen($path)) ? $path : $this->request->path;

		if (preg_match('/^\{([\w-]+)\}$/', $path, $match)) {
			$redirectTo = $this->settings->redirections->get($match[1], $redirectTo);
		}

		header('Location: ' . $redirectTo);
	}

	public function log($text = '', $time = FALSE) {
		$this->loggedMessages++;

		$f   = fopen('log.txt', 'a+');
		$ts  = round(microtime(TRUE), 4);
		$tf  = floor($ts);
		$td  = round($ts - $tf, 3);
		$td  = substr($td, 2);
		$tm  = '>> ';
		$tm .= ($time) ? date('Y-m-d H:i:s', $tf) . "." . $td . ":\n" : '';

		$text = (is_object($text) || is_array($text)) ? json_encode($text) : $text;
		fwrite($f, $tm . $text . "\n");
		fclose($f);
	}
}

class Object {
	public function __construct($data = []) {
		$this->fromArray($data);
	}

	public function get($variable, $default = NULL) {
		if (isset($this->{$variable})) {
			return (is_string($this->{$variable})) ? $this->unescape($this->{$variable}) : $this->{$variable};
		}
		else {
			return $default;
		}
	}

	public function set($variable, $value) {
		$this->{$variable} = is_string($value) ? $this->escape($value) : $value;

		return ($this->has($variable));
	}

	public function del($variable) {
		unset($this->{$variable});

		return !$this->has($variable);
	}

	public function has($variable) {
		$variables = get_object_vars($this);
		return array_key_exists($variable, $variables);
	}

	public function isEmpty() {
		return (count(get_object_vars($this))) ? FALSE : TRUE;
	}

	public function fromArray($data) {
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				$this->{$key} = new \Clair\Object($value);
			}
			else {
				$this->set($key, $value); 
			}
		}
	}

	public function toArray() {
		$data = get_object_vars($this);
		foreach ($data as $key => $value) {
			if (is_a($value, '\\Clair\\Object')) {
				$data[$key] = $value->toArray();
			}
		}
		return $data;
	}

	public function fromJSON($data) {
		$decoded = json_decode($data, TRUE);
		try {
			if (is_null($decoded))
				throw (new \Exception('Argument 1 in fromJSON() method is not valid JSON string.'));

			$this->fromArray($decoded);
		}
		catch(\Exception $e) {
			print_r($e->getMessage());
		}
	}

	public function toJSON() {
		return json_encode($this->toArray());
	}

	private function escape($data) {
		return addslashes($data);
	}

	private function unescape($data) {
		return stripslashes($data);
	}
}

class Session extends Object {
	public function __construct() {
		session_start();
		$this->load();
	}

	public function load() {
		$this->fromArray($_SESSION);
	}

	public function save() {
		$_SESSION = $this->toArray();
	}
}

class Request {
	public $url;
	public $path;
	public $query;
	public $method;
	public $isAjax  = FALSE;
	public $vars    = [];
	public $input   = [];
	public $headers = [];

	public function __construct() {
		$this->method = strtolower($_SERVER['REQUEST_METHOD']);
		$this->set($_SERVER["REQUEST_URI"]);

		$this->headers = apache_request_headers();
		$this->isAjax  = (isset($this->headers['X-Requested-With']) || isset($this->headers['is-ajax-request'])) ? TRUE : FALSE;
	}

	public function set($url) {
		$this->url = $url;
		$parsed    = parse_url($this->url);

		if (!isset($parsed['path']))
			$parsed['path'] = '';

		$parsed['path'] = explode('/', $parsed['path']);
		$this->vars     = [];
		
		foreach ($parsed['path'] as $pathPart) {
			if (preg_match('/\{(\*|\w+)\}/', $pathPart, $match)) {
				$this->vars[$match[1]] = '';
			}
		}
		$this->path = implode('/', $parsed['path']);

		if (!isset($parsed['query']))
			$parsed['query'] = '';

		parse_str($parsed['query'], $this->query);

		if ($this->method == 'put') {
			parse_str(file_get_contents('php://input'), $_POST);
		}

		if (count($_POST)) {
			foreach ($_POST as $key => $value) {
				$this->input[$key] = $value;
			}
			$_POST = [];
		}
	}
}

interface iController {
	public function get();
	public function put();
	public function post();
	public function delete();
	public function getTemplateData();
	public function middleware($alias);
	public function getMiddleware($beforeAfter);
	public function useMiddleware($controllerName, $alias, $beforeAfter);
}
interface iMiddleware {
	public function __construct(&$app, &$controller);
	public function get();
	public function put();
	public function post();
	public function delete();
}

class Controller implements iController {
	public $subcontrollers       = [];
	protected $middlewareAliases = [];
	protected $middlewareList    = array('before' => [], 'after' => []);
	protected $rootTemplate      = '';
	protected $templateName      = '';
	public $template;
	public $app;

	public function __construct(&$app) {
		$this->rootTemplate .= ($app->request->isAjax) ? 'ajax' : 'index';
		$this->templateName  = get_class($this);
		$this->template      = new \Clair\Object;
		$this->app           = $app;
	}

	public function get() {}
	public function put() {}
	public function post() {}
	public function delete() {}

	public function middleware($alias) {
		list($group, $name) = $this->middlewareAliases[$alias];
		return $this->middlewareList[$group][$name];
	}

	public function getMiddleware($beforeAfter) {
		return $this->middlewareList[$beforeAfter];
	}

	public function useMiddleware($middlewareName, $alias, $beforeAfter = NULL) {
		if ($beforeAfter === NULL)
			$beforeAfter = 'after';

		if (substr($middlewareName, 0, 1) !== '\\')
			$middlewareName = '\\' . __NAMESPACE__ . '\\' . $middlewareName;
		
		$wanabePath = $this->app->strings->changeSlashes($middlewareName);
		$pathParts  = [];

		foreach (explode('/', $wanabePath) as $part)
			$pathParts[] = $this->app->strings->dkr($part, 'underscore');

		$middlewarePath                                      = MIDDLEWARE . substr(implode('/', $pathParts), 1) . '.php';
		$this->middlewareList[$beforeAfter][$middlewareName] = $this->app->loadAndInit($middlewarePath, $middlewareName, $this);
		$this->middlewareAliases[$alias]                     = array($beforeAfter, $middlewareName);
	}

	public function setRootTemplate($rootTemplateName = '') {
		if (strlen($rootTemplateName)) {
			$this->rootTemplate = $rootTemplateName;
			return TRUE;
		}
		return FALSE;
	}

	public function getTemplateData() {
		return array($this->rootTemplate, $this->templateName, $this->template);
	}

	public function useController($controllerName, $alias) {
		$controllerFile = substr(strtolower($this->app->strings->dkr($this->app->strings->changeSlashes($controllerName), 'underscore')), 1);
		$controllerPath = CONTROLLERS . $controllerFile . '.php';
		$ctrl           = $this->app->loadAndInit($controllerPath, $controllerName);

		$ctrl->setRootTemplate('ajax');
		$this->subcontrollers[$alias] = $ctrl;
	}

	public function getController($alias) {
		return isset($this->subcontrollers[$alias]) ? $this->subcontrollers[$alias] : NULL;
	}
}

class Middleware implements iMiddleware {
	protected $app;

	public function __construct(&$app, &$controller) {
		$this->app        = $app;
		$this->controller = $controller;
	}

	public function get() {}
	public function put() {}
	public function post() {}
	public function delete() {}
}

class View {
	public function __construct(&$controller) {
		list($rootTemplate, $templateName, $template) = $controller->getTemplateData();
		$ctrlTemplatePath = VIEWS . strtolower($controller->app->strings->dkr($controller->app->strings->changeSlashes($templateName), 'underscore')) . '.html';
		$rootTemplatePath = VIEWS . $rootTemplate . '.html';
		$subCtrls         = [];
		$BODY             = '';

		foreach ($controller->subcontrollers as $alias => $ctrl) {
			ob_start();
			$ctrl->app->processRun($ctrl);
			$subCtrls[strtoupper($alias)] = ob_get_clean();
		}

		if (count($subCtrls)) {
			extract($subCtrls);
		}

		try {
			if ($controller->app->request->isAjax) {
				$BODY = json_encode($template->toArray());
			}
			else {
				if (!file_exists($ctrlTemplatePath))
					throw (new \Exception('Template "' . $ctrlTemplatePath . '" does not exist.'));

				extract($template->toArray());

				ob_start();
				include($ctrlTemplatePath);
				$BODY = ob_get_clean();
			}
		}
		catch (\Exception $e) {
			print_r($e->getMessage());
		}

		try {
			
			if (!file_exists($rootTemplatePath))
				throw (new \Exception('Root template "' . $rootTemplate . '" does not exist.'));

			extract($controller->app->settings->meta->toArray());

			ob_start();
			include($rootTemplatePath);
			echo ob_get_clean();
		}
		catch (\Exception $e) {
			print_r($e->getMessage());
		}
	}

	private function processTemplate($template) {

		$template = preg_replace('/\{\{#if +(.+)\}\}/', '<?php if ($1): ?>', $template);
		$template = preg_replace('/\{\{\/((if)|(for))\}\}/', '<?php end$1; ?>', $template);
		preg_match_all('/\{\{([\w\'\"-\[\]\(\)]+)\}\}/', $template, $matches, PREG_SET_ORDER);

		foreach ($matches as $match) {
			var_dump ($match);

			if (is_array(${$match[1]}))
				$template = str_replace($match[0], print_r(${$match[1]}), $template);
			else
				$template = str_replace($match[0], ${$match[1]}, $template);
		}
		
		preg_match_all('/\{\{([\w\'\"-\[\]\(\)]+) +\[\]\}\}/', $template, $matches, PREG_SET_ORDER);
		foreach ($matches as $match) {
			if (is_array(${$match[1]})) {
				$template = str_replace($match[0], implode('', ${$match[1]}), $template);
			}
		}
		var_dump ($matches);
		/*

		foreach ($match as $match) {
			str_replace($match[0], '<?php for(' . $match[1] . '): ?>', $template);
		}
		*/

		// echo '<pre>';
		// print_r(htmlspecialchars($template));
		// echo '</pre>';

		return $template;
	}
}