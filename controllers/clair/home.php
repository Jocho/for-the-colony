<?php
namespace Clair;
class Home extends \Clair\Controller {
	public function __construct(&$app) {
		parent::__construct($app);
		// $this->useMiddleware('Users');
		
	}
	public function get() {
		$this->template->set('hello', 'Hello World!');

		// $this->c();
		$this->r();
		$this->u();
		$this->d();
	}

	private function randomString($length = 12) {
		$t = '0123456789abcdefghijklmnopqrstuvwxyz_';
		$s = '';
		for ($i = 0; $i < $length; $i++) {
			$l  = substr($t, rand(0, 36), 1);
			$s .= rand(0, 1) ? strtoupper($l) : $l;
		}
		
		return $s;
	}

	private function c() {
		$id = $this->app->database
			->table('test')
			->show()
			->insert(array(
				'value' => $this->randomString()
			));

		var_dump ($id);
	}

	private function r() {
		$r = $this->app->database
			->table('test')
			->select('MIN(id) AS minimal')
			->show()
			->value('minimal');

		var_dump ($r);
	}

	private function u() {
		$r = $this->app->database
			->table('test')
			->where('id', 122)
			->show()
			->update(array('value' => '-'));

		var_dump ($r);
	}

	private function d() {
		$minimal = $this->app->database
			->table('test')
			->select('MIN(id) AS minimal')
			->show()
			->value('minimal');

		$min = reset($minimal);

		$r = $this->app->database
			->table('test')
			->where('id', $min)
			->show()
			->delete();
		
		var_dump ($r);
	}
}