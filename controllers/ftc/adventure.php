<?php
namespace Ftc;
class Adventure extends \Clair\Controller {
	public function __construct(&$app) {
		parent::__construct($app);
		$this->setRootTemplate('ajax');

		$this->template->errors = array();
		$this->user = ($this->app->session->has('user'))
			? $this->app->session->get('user')
			: new \Clair\Object;

		// $this->app->session->del('user');
	}

	public function get() {
		if ($this->user->isEmpty())
			return;

		$this->template->adventures = $this->getUserAdventures($this->user->id);
	}

	public function post() {
		
	}

	public function put() {
		if ($this->user->isEmpty())
			return;

		$in    = $this->app->request->input;
		$advs = $in['adventures'];

		if (empty($advs)) {
			$this->template->errors[] = 'No adventures to process.';
			return;
		}

		$dbIds = $this->getUserDbIds($this->user->id);

		foreach ($advs as $key => $adv) {
			if (isset($adv['dbid']) && in_array($adv['dbid'], $dbIds)) {
				$advs[$key] = $this->updateAdventure($adv);
			}
			elseif (!isset($adv['dbid'])) {
				$advs[$key] = $this->createAdventure($adv);
			}
			else {
				$this->template->errors[] = 'The adventure is not yours.';
			}
		}

		$this->template->adventures = $advs;
	}

	public function delete() {
		if ($this->user->isEmpty())
			return;

		$dbIds = $this->getUserDbIds($this->user->id);

		if (isset($this->app->request->vars['dbid']) && in_array($this->app->request->vars['dbid'], $dbIds))
			$this->deleteAdventure($this->app->request->vars['dbid']);
	}

	private function getAdventure($dbid) {
		$res = $this->app->database
			->table('adventures')
			->select('data')
			->where('dbid', $dbid)
			->first();

		return (strlen($res['data'])) ? $this->normalizeAdventure(json_decode($res['data'], TRUE)) : array();
	}

	private function getUserAdventures($userId) {
		$res = $this->app->database
			->table('adventures')
			->select('data')
			->where('user_id', $userId)
			->get();

		$advs = [];
		if (count($res)) {
			foreach ($res as $r)
				$advs[] = $this->normalizeAdventure(json_decode($r['data'], TRUE));
		}
		return $advs;
	}

	private function normalizeAdventure($adv) {
		if (!isset($adv['actors']))
			$adv['actors'] = [];

		if (!isset($adv['animals']['found']))
			$adv['animals']['found'] = [];

		foreach ($adv['actors'] as $k => $actor) {
			foreach ($actor['troubles'] as $kk => $trouble) {
				if (!isset($trouble['list']))
					$adv['actors'][$k]['troubles'][$kk]['list'] = [];
			}
		}
		return $adv;
	}

	private function getUserDbIds($userId) {
		$res = $this->app->database
			->table('adventures')
			->select('dbid')
			->where('user_id', $userId)
			->get();

		$dbIds = [];
		foreach ($res as $key => $r) {
			$dbIds[] = $r['dbid'];
		}
		return $dbIds;
	}

	private function updateAdventure($data) {
		$time = time();
		$data['changed'] = $time;
		$data['synced']  = $time;

		$this->app->database
			->table('adventures')
			->where('dbid', $data['dbid'])
			->update(array(
				'data'  => json_encode($data),
				'mdate' => $time
			));

		return $this->getAdventure($data['dbid']);
	}

	private function createAdventure($data) {
		$time = time();
		$dbid = md5($this->user->id . '_' . $data['id'] . time());

		$data['changed'] = $time;
		$data['synced']  = $time;
		$data['dbid']    = $dbid;

		$this->app->database
			->table('adventures')
			->insert(array(
				'data'    => json_encode($data),
				'mdate'   => $time,
				'dbid'    => $dbid,
				'user_id' => $this->user->id
			));

		return $this->getAdventure($data['dbid']);
	}

	private function deleteAdventure($dbid) {
		return $this->app->database
			->table('adventures')
			->where('dbid', $dbid)
			->delete();
	}
}