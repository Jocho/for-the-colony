<?php
namespace Ftc;
class Character extends \Clair\Controller {
	public function __construct(&$app) {
		parent::__construct($app);
		$this->setRootTemplate('ajax');

		$this->template->errors = array();
		$this->user = ($this->app->session->has('user'))
			? $this->app->session->get('user')
			: new \Clair\Object;

		// $this->app->session->del('user');
	}

	public function get() {
		if ($this->user->isEmpty())
			return;

		$this->template->characters = $this->getUserCharacters($this->user->id);
	}

	public function post() {
		
	}

	public function put() {
		if ($this->user->isEmpty())
			return;

		$in    = $this->app->request->input;
		$chars = $in['characters'];

		if (empty($chars)) {
			$this->template->errors[] = 'No characters to process.';
			return;
		}

		$dbIds = $this->getUserDbIds($this->user->id);

		foreach ($chars as $key => $char) {
			if (isset($char['dbid']) && in_array($char['dbid'], $dbIds)) {
				$chars[$key] = $this->updateCharacter($char);
			}
			elseif (!isset($char['dbid'])) {
				$chars[$key] = $this->createCharacter($char);
			}
			else {
				$this->template->errors[] = 'The character is not yours.';
			}
		}

		$this->template->characters = $chars;
	}

	public function delete() {
		if ($this->user->isEmpty())
			return;

		$dbIds = $this->getUserDbIds($this->user->id);

		if (isset($this->app->request->vars['dbid']) && in_array($this->app->request->vars['dbid'], $dbIds))
			$this->deleteCharacter($this->app->request->vars['dbid']);
	}

	private function getById($id) {
		return $this->app->database
			->table('characters')
			->select('*')
			->where('id', $id)
			->first();
	}

	private function getCharacter($dbid) {
		$res = $this->app->database
			->table('characters')
			->select('data')
			->where('dbid', $dbid)
			->first();

		return (strlen($res['data'])) ? $this->normalizeCharacter(json_decode($res['data'], TRUE)) : array();
	}

	private function getUserCharacters($userId) {
		$res = $this->app->database
			->table('characters')
			->select('data')
			->where('user_id', $userId)
			->get();

		$chars = [];
		if (count($res)) {
			foreach ($res as $r)
				$chars[] = $this->normalizeCharacter(json_decode($r['data'], TRUE));
		}
		return $chars;
	}

	private function normalizeCharacter($char) {
		if (!isset($char['abilities']))
			$char['abilities'] = [];

		foreach ($char['troubles'] as $k => $trouble) {
			if (!isset($trouble['list']))
				$char['troubles'][$k]['list'] = [];
		}
		return $char;
	}

	private function getUserDbIds($userId) {
		$res = $this->app->database
			->table('characters')
			->select('dbid')
			->where('user_id', $userId)
			->get();

		$dbIds = [];
		foreach ($res as $key => $r) {
			$dbIds[] = $r['dbid'];
		}
		return $dbIds;
	}

	private function updateCharacter($data) {
		$time = time();
		$data['changed'] = $time;
		$data['synced']  = $time;

		$this->app->database
			->table('characters')
			->where('dbid', $data['dbid'])
			->update(array(
				'data'  => json_encode($data),
				'mdate' => $time
			));

		return $this->getCharacter($data['dbid']);
	}

	private function createCharacter($data) {
		$time = time();
		$dbid = md5($this->user->id . '_' . $data['id'] . time());

		$data['changed'] = $time;
		$data['synced']  = $time;
		$data['dbid']    = $dbid;

		$this->app->database
			->table('characters')
			->insert(array(
				'data'    => json_encode($data),
				'mdate'   => $time,
				'dbid'    => $dbid,
				'user_id' => $this->user->id
			));

		return $this->getCharacter($data['dbid']);
	}

	private function deleteCharacter($dbid) {
		return $this->app->database
			->table('characters')
			->where('dbid', $dbid)
			->delete();
	}
}