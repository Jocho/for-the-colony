<?php
namespace Ftc;
class Home extends \Clair\Controller {
	public function __construct(&$app) {
		parent::__construct($app);
		$this->useMiddleware('\Translatie\Translatie', 'translatie', 'before');

		if ($this->app->session->has('lang')) {
			$lang = $this->app->session->get('lang');
		}
		else {
			$acceptLang = explode(';', $this->app->request->headers['Accept-Language']);
			$language   = explode(',', $acceptLang[0]);
			$lang       = $language[1];
		}

		$t = new \Translatie\Translatie;
		$t->setTranslationsFolder(ROOT . 'res/languages/');
		$t->setLanguage($lang);
		$this->template->currentLang = $lang;
		$this->template->t = $t;

		// $this->app->session->del('lang');

		// var_dump ($this->app->session);
	}

	public function get() {
		if (isset($this->app->request->query['lang'])) {
			$this->app->session->set('lang', $this->app->request->query['lang']);
			$this->app->redirect('/');
		}
	}
}