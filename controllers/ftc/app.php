<?php
namespace Ftc;
class App extends \Clair\Controller {
	public function __construct(&$app) {
		parent::__construct($app);
		$this->useMiddleware('\Translatie\Translatie', 'translatie', 'before');

		if ($this->app->session->has('lang')) {
			$lang = $this->app->session->get('lang');
		}
		else {
			$acceptLang = explode(';', $this->app->request->headers['Accept-Language']);
			$language   = explode(',', $acceptLang[0]);
			$lang       = $language[1];
		}

		$t = new \Translatie\Translatie;
		$t->setTranslationsFolder(ROOT . 'res/languages/');
		$t->setLanguage($lang);
		$this->template->t = $t;

		$this->setRootTemplate('app');
	}

	public function get() {
		$t = $this->template->t;
		$this->template->tran = new \Clair\Object([
			'animalData' => [
				['name' => $t->_('Bug'),        'vitality' => 1,  'energy' => 1],
				['name' => $t->_( 'Butterfly'), 'vitality' => 2,  'energy' => 2],
				['name' => $t->_( 'Gerbil'),    'vitality' => 3,  'energy' => 3],
				['name' => $t->_( 'Bat'),       'vitality' => 3,  'energy' => 4],
				['name' => $t->_( 'Hamster'),   'vitality' => 4,  'energy' => 2],
				['name' => $t->_( 'Mouse'),     'vitality' => 4,  'energy' => 3],
				['name' => $t->_( 'Squirell'),  'vitality' => 4,  'energy' => 3],
				['name' => $t->_( 'Mole'),      'vitality' => 5,  'energy' => 1],
				['name' => $t->_( 'Bullfrog'),  'vitality' => 5,  'energy' => 2],
				['name' => $t->_( 'Rat'),       'vitality' => 5,  'energy' => 3],
				['name' => $t->_( 'Hedgehog'),  'vitality' => 6,  'energy' => 2],
				['name' => $t->_( 'Beaver'),    'vitality' => 7,  'energy' => 2],
				['name' => $t->_( 'Hare'),      'vitality' => 7,  'energy' => 3],
				['name' => $t->_( 'Racoon'),    'vitality' => 8,  'energy' => 2],
				['name' => $t->_( 'Tortoise'),  'vitality' => 20, 'energy' => 1],

				['name' => $t->_( 'Crab'),      'vitality' => 3,  'energy' => 1],
				['name' => $t->_( 'Scorpion'),  'vitality' => 4,  'energy' => 2],
				['name' => $t->_( 'Tarantula'), 'vitality' => 5,  'energy' => 3],
				['name' => $t->_( 'Mink'),      'vitality' => 8,  'energy' => 2],
				['name' => $t->_( 'Weasel'),    'vitality' => 8,  'energy' => 3],
				['name' => $t->_( 'Snake'),     'vitality' => 9,  'energy' => 2],
				['name' => $t->_( 'Falcon'),    'vitality' => 9,  'energy' => 3],
				['name' => $t->_( 'Hawk'),      'vitality' => 10, 'energy' => 4],
				['name' => $t->_( 'Owl'),       'vitality' => 10, 'energy' => 4],
				['name' => $t->_( 'Cat'),       'vitality' => 11, 'energy' => 2],
				['name' => $t->_( 'Fox'),       'vitality' => 12, 'energy' => 2],
				['name' => $t->_( 'Lynx'),      'vitality' => 13, 'energy' => 2],
				['name' => $t->_( 'Badger'),    'vitality' => 12, 'energy' => 2],
				['name' => $t->_( 'Wolf'),      'vitality' => 15, 'energy' => 2],
				['name' => $t->_( 'Bear'),      'vitality' => 20, 'energy' => 2]
			],

			'mouseAbilities' => [
				['id' => 0,  'active' => 1, 'name' => $t->_('Ripper'),        'description' => $t->_('Perform attack with a 1d6 / 2 (round down) bonus.')],
				['id' => 1,  'active' => 1, 'name' => $t->_('Teritorialist'), 'description' => $t->_('Selected enemy fails in next round already at 1-3 result.')],
				['id' => 2,  'active' => 1, 'name' => $t->_('Squeaker'),      'description' => $t->_('Select an enemy that performs only 1 action next round.')],
				['id' => 3,  'active' => 1, 'name' => $t->_('Sweeper'),       'description' => $t->_('A pursuer loses trail of whole group.')],
				['id' => 4,  'active' => 1, 'name' => $t->_('Manipulator'),   'description' => $t->_('Persuade a creature of similar size to cooperate with the group for 1d6 rounds.')],

				['id' => 5,  'active' => 1, 'name' => $t->_('Searcher'),      'description' => $t->_('Find a 1d6 / 2 food or water portions (by environment).')],
				['id' => 6,  'active' => 1, 'name' => $t->_('Cook'),          'description' => $t->_('Create a portion of meal from any source.')],
				['id' => 7,  'active' => 1, 'name' => $t->_('Healer'),        'description' => $t->_('Create a healing meal from any food.')],
				['id' => 8,  'active' => 1, 'name' => $t->_('Infectious'),    'description' => $t->_('When ill, you may grant one healthy enemy with single disease trouble.')],
				['id' => 9,  'active' => 1, 'name' => $t->_('Resistant'),     'description' => $t->_('If you should fall ill, throw 1d6 and get sick only on 1-3.')],

				['id' => 10, 'active' => 1, 'name' => $t->_('Unbeatable'),    'description' => $t->_('Can swap single wound or disease for exhaustion.')],
				['id' => 11, 'active' => 1, 'name' => $t->_('Motivator'),     'description' => $t->_('A group throws dice whole round without penalties.')],
				['id' => 12, 'active' => 1, 'name' => $t->_('Unhearable'),    'description' => $t->_('A group may talk without noise.')],
				['id' => 13, 'active' => 1, 'name' => $t->_('Hider'),         'description' => $t->_('Until someone talks or moves is considered hidden.')],
				['id' => 14, 'active' => 1, 'name' => $t->_('Sleuth'),        'description' => $t->_('Identify traces, understands marks and writings.')],

				// passive
				['id' => 15, 'active' => 0, 'name' => $t->_('Handy'),          'description' => $t->_('Manipulate items with advantage.')],
				['id' => 16, 'active' => 0, 'name' => $t->_('Muzzle'),         'description' => $t->_('Smell-related actions perform with advantage.')],
				['id' => 17, 'active' => 0, 'name' => $t->_('Bunny'),          'description' => $t->_('Listening actions perform with advantage.')],
				['id' => 18, 'active' => 0, 'name' => $t->_('Toothie'),        'description' => $t->_('Biting the items perform with advantage.')],
				['id' => 19, 'active' => 0, 'name' => $t->_('Swimmer'),        'description' => $t->_('Swimming/diving actions perform with advantage.')],

				['id' => 20, 'active' => 0, 'name' => $t->_('Courage'),        'description' => $t->_('Actions on large enemies perform with advantage.')],
				['id' => 21, 'active' => 0, 'name' => $t->_('Runner'),         'description' => $t->_('You run 2 times faster than average mouse.')],
				['id' => 22, 'active' => 0, 'name' => $t->_('Climber'),        'description' => $t->_('Climb a surface where no one can catch.')],
				['id' => 23, 'active' => 0, 'name' => $t->_('Flexible'),       'description' => $t->_('Get through a hole with diameter of an average pen.')],
				['id' => 24, 'active' => 0, 'name' => $t->_('Fluffy'),         'description' => $t->_('Next 1d6 actions may perform without any noise.')],

				['id' => 25, 'active' => 0, 'name' => $t->_('Economic'),       'description' => $t->_('It may choose to remove 1 hunger point during Rest.')],
				['id' => 26, 'active' => 0, 'name' => $t->_('Feeler'),         'description' => $t->_('Senses if a group is in danger for next 1d6 × 10 min.')],
				['id' => 27, 'active' => 0, 'name' => $t->_('Carrier'),        'description' => $t->_('Carries 2 times heavier burden than its own weight.')],
				['id' => 28, 'active' => 0, 'name' => $t->_('Adrenaline'),     'description' => $t->_('During duels gets no penalty for exhaustion.')],
				['id' => 29, 'active' => 0, 'name' => $t->_('Metabolist'),     'description' => $t->_('Rest 2 times faster.')]
			],

			'divisions' => [
				$t->_('- No Division -'),
				$t->_('Harvester'),
				$t->_('Scout'),
				$t->_('Tuft'),
				$t->_('Fighter')
			],

			'sceneTabs' => [
				$t->_('General'),
				$t->_('NPCs'),
				$t->_('Places'),
				$t->_('Quests')
			],

			'texts' => [
				'newScene' => $t->_('New Scene')
			]
		]);
	}

	public function post() {
	
	}

	public function put() {
	
	}

	public function delete() {
	
	}
}