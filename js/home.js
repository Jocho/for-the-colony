var Main = (function() {
	_m = {};

	var menuOffset = 0;

	_m.elems = {
		menu: document.querySelector('.menu')
	}

	var init = function() {
		initScroll();
	};

	var initScroll = function() {
		menuOffset = _m.elems.menu.offsetTop;

		window.addEventListener('scroll', function() {
			if (window.scrollY > menuOffset)
				_m.elems.menu.classList.add('menu--fixed');
			else
				_m.elems.menu.classList.remove('menu--fixed');
		})
	};

	init();

	return _m;
});

window.addEventListener('load', function() {
	var main = Main();
});