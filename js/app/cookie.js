var Cookie = (function() {
    var _c = {};
    var cookieList = {};
    
    /**
     * Parses actual document.cookie variable
     */
    var init = function() {
        var cookies = document.cookie.split('; ');
        var ii = cookies.length;
        for (var i = 0; i < ii; i++) {
            var x = cookies[i].split('=');
            var key = x.shift();
            cookieList[key] = x.join('=');
        }
    };
    
    /**
     * Returns list of actual cookies
     */
    _c.list = function() {
        return cookieList;
    };

    _c.reinit = function() {
        init();
    }

    /**
     * Returns value of requested cookie
     * @param string variable - cookie variable name to be requested
     * @return value of requested cookie variable. If cookie does not exist, null is returned.
     */
    _c.get = function(variable) {
        return (typeof cookieList[variable] !== 'undefined') ? cookieList[variable] : null;
    };

    /**
     * Sets up new cookie value
     * @param string variable - name of cookie variable to be set
     * @param string value - value of cookie being set
     * @param mixed expires - date of expiration given in a format processed by Date object
     * @param string path - string of url address, where the cookie is available
     * @return
     */
    _c.set = function(variable, value, expires, path) {
        expires = (typeof expires !== 'undefined') ? expires : '';
        path = (typeof path !== 'undefined') ? path : '/';

        if (typeof value === 'undefined') {
            console.error('Please, set a value to cookie.');
            return null;
        }

        if (typeof variable === 'undefined') {
            console.error('Cookie has to be named and given a value.');
            return null;
        }

        var cookie = variable + '=' + value;
        if (String(expires).length) {
            var d = new Date(expires);
            cookie += '; expires=' + d.toGMTString();
        }

        if (path.length) {
            cookie += '; path=' + path;
        }

        document.cookie = cookie;
        cookieList[variable] = value;
    };

    /**
     * Deletes cookie variable
     * @param string variable - name of cookie variable to be deleted
     */
    _c.del = function(variable) {
        delete(cookieList[variable]);
        document.cookie = variable + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
    };

    /**
     * Checks whether a requested cookie exists or not.
     * @param string variable - cookie variable name
     * @return boolean - true if requested cookie exists, false otherwise
     */
    _c.exists = function(variable) {
        return typeof cookieList[variable] !== 'undefined';
    }

    init();
    return _c;
});