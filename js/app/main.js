/*
const ANIMALDATA = [
	{name: 'Bug',        vitality: 1,  energy: 1},
	{name: 'Butterfly',  vitality: 2,  energy: 2},
	{name: 'Gerbil',     vitality: 3,  energy: 3},
	{name: 'Bat',        vitality: 3,  energy: 4},
	{name: 'Hamster',    vitality: 4,  energy: 2},
	{name: 'Mouse',      vitality: 4,  energy: 3},
	{name: 'Squirell',   vitality: 4,  energy: 3},
	{name: 'Mole',       vitality: 5,  energy: 1},
	{name: 'Bullfrog',   vitality: 5,  energy: 2},
	{name: 'Rat',        vitality: 5,  energy: 3},
	{name: 'Hedgehog',   vitality: 6,  energy: 2},
	{name: 'Beaver',     vitality: 7,  energy: 2},
	{name: 'Hare',       vitality: 7,  energy: 3},
	{name: 'Racoon',     vitality: 8,  energy: 2},
	{name: 'Tortoise',   vitality: 20, energy: 1},

	{name: 'Crab',       vitality: 3,  energy: 1},
	{name: 'Scorpion',   vitality: 4,  energy: 2},
	{name: 'Tarantula',  vitality: 5,  energy: 3},
	{name: 'Mink',       vitality: 8,  energy: 2},
	{name: 'Weasel',     vitality: 8,  energy: 3},
	{name: 'Snake',      vitality: 9,  energy: 2},
	{name: 'Falcon',     vitality: 9,  energy: 3},
	{name: 'Hawk',       vitality: 10, energy: 4},
	{name: 'Owl',        vitality: 10, energy: 4},
	{name: 'Cat',        vitality: 11, energy: 2},
	{name: 'Fox',        vitality: 12, energy: 2},
	{name: 'Lynx',       vitality: 13, energy: 2},
	{name: 'Badger',     vitality: 12, energy: 2},
	{name: 'Wolf',       vitality: 15, energy: 2},
	{name: 'Bear',       vitality: 20, energy: 2}
];

const MOUSEABILITIES = [
	// active
	{id: 0, active: 1, name: 'Ripper', description: 'Perform attack with a 1d6 / 2 (round down) bonus.'},
	{id: 1, active: 1, name: 'Teritorialist', description: 'Selected enemy fails in next round already at 1-3 result.'},
	{id: 2, active: 1, name: 'Squeaker', description: 'Select an enemy that performs only 1 action next round.'},
	{id: 3, active: 1, name: 'Sweeper', description: 'A pursuer loses trail of whole group.'},
	{id: 4, active: 1, name: 'Manipulator', description: 'Persuades a creature of similar size to cooperate with the group for 1d6 rounds.'},
	
	{id: 5, active: 1, name: 'Searcher', description: 'Find a 1d6 / 2 food or water portions (by environment).'},
	{id: 6, active: 1, name: 'Cook', description: 'Create a portion of meal from any source.'},
	{id: 7, active: 1, name: 'Healer', description: 'Create a healing meal from any food.'},
	{id: 8, active: 1, name: 'Infectious', description: 'When ill, you may grant one healthy enemy with single disease trouble.'},
	{id: 9, active: 1, name: 'Resistant', description: 'If you should fall ill, throw 1d6 and get sick only on 1-3.'},

	{id: 10, active: 1, name: 'Unbeatable', description: 'Can swap single wound or disease for exhaustion.'},
	{id: 11, active: 1, name: 'Motivator', description: 'A group throws dice whole round without penalties.'},
	{id: 12, active: 1, name: 'Unhearable', description: 'A group may talk without noise.'},
	{id: 13, active: 1, name: 'Hider', description: 'Until someone talks or moves is considered hidden.'},
	{id: 14, active: 1, name: 'Sleuth', description: 'Identify traces, understands marks and writings.'},
	
	// passive
	{id: 15, active: 0, name: 'Handy', description: 'Manipulate items with advantage.'},
	{id: 16, active: 0, name: 'Muzzle', description: 'Smell-related actions perform with advantage.'},
	{id: 17, active: 0, name: 'Bunny', description: 'Listening actions perform with advantage.'},
	{id: 18, active: 0, name: 'Toothie', description: 'Biting the items perform with advantage.'},
	{id: 19, active: 0, name: 'Swimmer', description: 'Swimming/diving actions perform with advantage.'},
	
	{id: 20, active: 0, name: 'Courage', description: 'Actions on large enemies perform with advantage.'},
	{id: 21, active: 0, name: 'Runner', description: 'You run 2 times faster than average mouse.'},
	{id: 22, active: 0, name: 'Climber', description: 'Climb a surface where no one can catch.'},
	{id: 23, active: 0, name: 'Flexible', description: 'Get through a hole with diameter of an average pen.'},
	{id: 24, active: 0, name: 'Fluffy', description: 'Next 1d6 actions may perform without any noise.'},
	
	{id: 25, active: 0, name: 'Economic', description: 'It may choose to remove 1 hunger point during Rest.'},
	{id: 26, active: 0, name: 'Feeler', description: 'Senses if a group is in danger for next 1d6 × 10 min.'},
	{id: 27, active: 0, name: 'Carrier', description: 'Carries 2 times heavier burden than its own weight.'},
	{id: 28, active: 0, name: 'Adrenaline', description: 'During duels gets no penalty for exhaustion.'},
	{id: 29, active: 0, name: 'Metabolist', description: 'Rest 2 times faster.'}
];

const DIVISIONS = ['- No Division -', 'Harvester', 'Scout', 'Tuft', 'Fighter'];
const SCENETABS = ['General', 'NPCs', 'Places', 'Quests'];

const TEXTS = {
	newScene: 'New Scene'
};
*/
const MOUSEABILITYAMOUNT = 4;


var cookie = new Cookie();

var sign = app.section('sign');

if (!sign.ctrl.isset('toggleSign'))
	sign.ctrl.set('toggleSign', 0);

sign.call.register('toggleSign', '_', function() {
	var val = sign.ctrl.get('toggleSign');
	sign.ctrl.set('toggleSign', val ? 0 : 1, false);

	var html0 = document.querySelector('.toggle-sign');
	var html1 = document.querySelector('.sign-title');
	var html2 = document.querySelectorAll('.signin-only');
	var html3 = document.querySelectorAll('.signup-only');

	if (val) {
		html0.text = 'Sign Up';
		html1.innerHTML = 'Sign Up';
		for (var i = 0; i < html2.length; i++) {
			html2[i].classList.add('hidden');
		}

		for (var i = 0; i < html3.length; i++) {
			html3[i].classList.remove('hidden');
		}
	}
	else {
		html0.text = 'Sign In';
		html1.innerHTML = 'Sign In';
		for (var i = 0; i < html2.length; i++) {
			html2[i].classList.remove('hidden');
		}

		for (var i = 0; i < html3.length; i++) {
			html3[i].classList.add('hidden');
		}
	}
});

sign.call.register('signUp', '_', function() {
	var email = sign.ctrl.get('email');
	var pass1 = sign.ctrl.get('password');
	var pass2 = sign.ctrl.get('password2');

	app.ajaxer.post('/users', {
		email: email,
		password: pass1,
		password2: pass2
	}, function(r) {
		var res = JSON.parse(r);
		if (!res.errors.length) {
			main.loadData();
			main.toggleLoginButton(true);
			document.querySelector('#sign .close').click();
		}
	});
});

sign.call.register('signIn', '_', function() {
	var email = sign.ctrl.get('email');
	var pass1 = sign.ctrl.get('password');

	app.ajaxer.put('/users', {
		email: email,
		password: pass1,
		action: 'login'
	}, function(r) {
		var res = JSON.parse(r);
		if (!res.errors.length) {
			main.loadData();
			main.toggleLoginButton(true);
			document.querySelector('#sign .close').click();
		}
	});
});

sign.call.run('toggleSign', '_');

var settings = app.section('settings');

if (!settings.ctrl.isset('showDice'))
	settings.ctrl.set('showDice', 1);

var footer = app.section('footer');
footer.call.register('logout', '_', function() {
	app.ajaxer.put('/users', {action: 'logout'}, function(r) {
		var res = JSON.parse(r);

		if (typeof res.user.email == 'undefined') {
			cookie.del('user_token');
			main.toggleLoginButton(false);
		}
	});
});







var main = (function(menu, character, adventure) {
	var _m = {};

	_m.autoresizeHandler = function(elem) {
		elem.style.height = 'auto';
		elem.style['overflow-y'] = 'hidden';
		elem.style.height = elem.scrollHeight + 'px';
	};

	_m.initAutoresizeTextarea = function() {
		var tas = document.querySelectorAll('.textarea--autoresize');

		for (var i = 0; i < tas.length; i++) {
			if (typeof tas[i].dataset.autoresized === 'undefined') {
				tas[i].addEventListener('input', function() {
					_m.autoresizeHandler(this);
				});
			}

			_m.autoresizeHandler(tas[i]);
		}
	};

	_m.init = function() {
		_m.initMenu();
		_m.initAutoresizeTextarea();
		_m.showSections();
		_m.setYear();
		// diceHelper.updateRollColors();
		// diceHelper.updateIcon();
		_m.loadData();
		_m.toggleLoginButton();
	};

	_m.initMenu = function() {
		menu.view.show('block');
		character.view.hide();
		adventure.view.hide();
		_m.toggleDice(false);
		_m.toggleFooter(true);
		window.location.hash = '';
	};

	_m.initCharacter = function() {
		menu.view.hide();
		_m.toggleDice(true);
		_m.toggleFooter(false);
		dice.ctrl.set('limit', ANIMALDATA[5].energy);
		character.view.show('block');
		adventure.view.hide();
		character.view.refresh();
		characterHelper.updatePenalties();
		characterHelper.updateAbilityTypes();
		window.location.hash = 'character';
	};

	_m.initAdventure = function() {
		menu.view.hide();
		_m.toggleDice(true);
		_m.toggleFooter(false);
		character.view.hide();
		adventure.view.show('block');
		adventureHelper.initPenalties();
		adventureHelper.initScene(advScenes.ctrl.get('id'));
		advScenes.view.refresh();
		adventure.view.refresh();
		window.location.hash = 'adventure';
	};

	_m.toggleFooter = function(state) {
		var html = document.querySelector('.footer');
		if (state)
			html.classList.remove('hidden');
		else
			html.classList.add('hidden');
	}

	_m.toggleDice = function(state) {
		var html = document.querySelector('.footer');
		if (state && settings.ctrl.get('showDice'))
			dice.view.show('block');
		else
			dice.view.hide();
	}

	_m.showSections = function() {
		var htmls = document.querySelectorAll('.section');

		for (var i = 0; i < htmls.length; i++)
			htmls[i].classList.remove('section--hidden');

		document.querySelector('.dice').classList.remove('hidden');
	};

	_m.setYear = function() {
		var d = new Date();
		document.querySelector('.year').innerHTML = d.getFullYear();
	}

	_m.getTimestamp = function() {
		var d = new Date();
		return Math.round(d.getTime() / 1000);
	}

	_m.isOnline = function() {
		return window.navigator.onLine;
	};

	_m.loadData = function() {
		if (!_m.isOnline() || !_m.isLogged())
			return;

		_m.createSession(function() {
			_m.loadCharacters(function() {
				_m.loadAdventures();
			});
		});
	}

	_m.createSession = function(callback) {
		if (_m.isLogged())
			app.ajaxer.put('/users', {action: 'at_login', access_token: cookie.get('user_token')}, function(r) {
				var res = JSON.parse(r);
				
				if (!res.errors.length)
					callback();
			});
	};

	_m.isLogged = function() {
		cookie.reinit();
		return cookie.exists('user_token');
	};

	_m.toggleLoginButton = function(state) {
		state = typeof state !== 'undefined' ? state : cookie.exists('user_token');

		var html1 = document.querySelector('.sign-in');
		var html2 = document.querySelector('.sign-out');

		if (state) {
			html1.classList.add('hidden');
			html2.classList.remove('hidden');
		}
		else {
			html1.classList.remove('hidden');
			html2.classList.add('hidden');
		}
	}

	_m.loadCharacters = function(callback) {
		app.ajaxer.get('/characters', function(r) {
			var res = JSON.parse(r);
			if (typeof res.characters !== 'undefined') {
				var chars = menu.ctrl.get('characters');
				
				res.characters.reduce(function(x, char) {
					var changed = parseInt(char.changed);
					var newOne = true;
					
					chars = chars.reduce(function(xx, yy) {
						if (char.dbid == yy.dbid) {
							newOne = false;
							var toPush = (changed > parseInt(yy.changed)) ? char : yy;
							xx.push(toPush);
						}
						else {
							xx.push(yy);
						}
						return xx;
					}, []);

					if (newOne) {
						chars.push(char);
					}

					return x;
				}, []);

				for (var i = 0, ii = chars.length; i < ii; i++) {
					chars[i].id = i;
				}

				menu.ctrl.set('characters', chars);
				callback();
			}
		});
	};

	_m.loadAdventures = function() {
		app.ajaxer.get('/adventures', function(r) {
			var res = JSON.parse(r);
			if (typeof res.adventures !== 'undefined') {
				var advs = menu.ctrl.get('adventures');
				
				res.adventures.reduce(function(x, adv) {
					var changed = parseInt(adv.changed);
					var newOne = true;
					
					advs = advs.reduce(function(xx, yy) {
						if (adv.dbid == yy.dbid) {
							newOne = false;
							var toPush = (changed > parseInt(yy.changed)) ? adv : yy;
							xx.push(toPush);
						}
						else {
							xx.push(yy);
						}
						return xx;
					}, []);

					if (newOne) {
						advs.push(adv);
					}

					return x;
				}, []);

				for (var i = 0, ii = advs.length; i < ii; i++) {
					advs[i].id = i;
				}

				menu.ctrl.set('adventures', advs);
			}
		});
	};

	_m.deleteCharacter = function(dbid) {
		app.ajaxer.delete('/characters/' + dbid, function(r) {
			var res = JSON.parse(r);
		});
	};

	_m.deleteAdventure = function(dbid) {
		app.ajaxer.delete('/adventures/' + dbid, function(r) {
			var res = JSON.parse(r);
		});
	};

	_m.saveCharacters = function() {
		app.ajaxer.put('/characters', {characters: menu.ctrl.get('characters')}, function(r) {
		var res = JSON.parse(r);
		if (typeof res.characters !== 'undefined')
			menu.ctrl.set('characters', res.characters);
		});
	};

	_m.saveAdventures = function() {
		app.ajaxer.put('/adventures', {adventures: menu.ctrl.get('adventures')}, function(r) {
		var res = JSON.parse(r);
		if (typeof res.adventures !== 'undefined')
			menu.ctrl.set('adventures', res.adventures);
		});
	};

	return _m;
}(menu, character, adventure));


window.addEventListener('load', function() {
	main.init();
	window.onhashchange = function() {
		var chHidden = character.view.isHidden();
		var aHidden = adventure.view.isHidden();
		if (!window.location.hash.length && (!chHidden || !aHidden)) {
			if (chHidden)
				adventure.call.run('saveAdventure', '_');
			else if (aHidden)
				character.call.run('saveCharacter', '_');
		}
	};
});