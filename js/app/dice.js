var dice = app.section('dice');

dice.ctrl.set({
	dieIcon: 'die',
	limit: 3,
	rolls: []
});

dice.call.register('limit', '_', function() {
	diceHelper.updateIcon();
});

dice.call.register('roll', '_', function() {
	var rolls = dice.ctrl.get('rolls');
	if (!diceHelper.canRoll()) {
		dice.ctrl.set('rolls', []);
		diceHelper.updateIcon();
		return;
	}

	rolls.push({
		value: diceHelper.roll(),
		rerolled: 0
	});
	dice.ctrl.set('rolls', rolls);
	diceHelper.updateIcon();
	diceHelper.updateRollColors();
});

dice.call.register('+reroll', '_', function(x) {
	if (diceHelper.canReroll(x[1])) {
		dice.ctrl.set('rolls.' + x[1], {
			value: diceHelper.roll(),
			rerolled: 1
		});
		diceHelper.updateRollColors();
	}
});

class DiceHelper {

	isTouchDevice() {
		if (this.touchDevice !== null)
			return this.touchDevice;

		this.touchDevice = 'ontouchstart' in window || navigator.maxTouchPoints;
		return this.touchDevice;
	}

	preventClick(b) {
		this.prevented = b;
	}

	constructor() {
		var down, up;
		this.touchDevice = null;
		this.prevented = false;
		this.timer;

		if (this.isTouchDevice()) {
			down = 'touchstart';
			up = 'touchend';
		}
		else {
			down = 'mousedown';
			up = 'mouseup';
		}

		var me = this;
		var elem = document.querySelector('.die');
		
		elem.addEventListener(down, function(e) {
			me.prevented = false;
			me.timer = setTimeout(function() {
				e.preventDefault();
				e.stopPropagation();
				me.preventClick(true);
				dice.ctrl.set('rolls', []);
			}, 666);
		});

		elem.addEventListener(up, function(e) {
			clearTimeout(me.timer);
			if (me.prevented) {
				e.preventDefault();
				e.stopPropagation();
			}
		})
	}

	updateIcon() {
		var icon = 'die';
		var rolls = dice.ctrl.get('rolls');
		var limit = dice.ctrl.get('limit');

		if (rolls.length == limit)
			icon = 'new-turn';

		if (limit <= 0)
			icon = 'scratch';

		dice.ctrl.set('dieIcon', icon);
	}

	roll() {
		return Math.floor(Math.random() * 6) + 1;
	}

	canRoll() {
		var rolls = dice.ctrl.get('rolls');
		var limit = dice.ctrl.get('limit');

		return !this.prevented && rolls.length + 1 <= parseInt(limit);
	}

	canReroll(id) {
		var rolls = dice.ctrl.get('rolls');
		return !rolls[id].rerolled;
	}

	updateRollColors() {
		var rolls = dice.ctrl.get('rolls');
		var elems = document.querySelectorAll('.dice-result__value');

		for (var i = 0; i < rolls.length; i++) {
			elems[i].classList.remove('dice-result__value--rerolled');
			if (rolls[i].rerolled)
				elems[i].classList.add('dice-result__value--rerolled');
		}
	}
}

var diceHelper = new DiceHelper;