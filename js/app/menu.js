var menu = app.section('menu');

if (!menu.ctrl.isset('characters'))
	menu.ctrl.set('characters', []);

if (!menu.ctrl.isset('adventures'))
	menu.ctrl.set('adventures', []);

menu.call.register('add+', '_', function(x) {
	switch (x[1]) {
		case 'character':
			character.ctrl.del('dbid');
			character.ctrl.set(characterHelper.getNewMouse());
			main.initCharacter();
			break;

		case 'adventure':
			adventure.ctrl.del('dbid');
			var adv = adventureHelper.getNewAdventure();

			advScenes.ctrl.set(adv.scenes, false);
			advActors.ctrl.set('actors', adv.actors);

			delete(adv.scenes);
			delete(adv.actors);

			adventure.ctrl.set(adv);
			main.initAdventure();
			break;
	}
});

menu.call.register('+load', '_', function(x) {
	switch (x[0]) {
		case 'characters':
			character.ctrl.set(menu.ctrl.get('characters.' + x[1]));
			main.initCharacter();
			break;
		case 'adventures':
			var adv = menu.ctrl.get('adventures.' + x[1])
			advScenes.ctrl.set(adv.scenes, false);
			advActors.ctrl.set('actors', adv.actors);

			delete(adv.scenes);
			delete(adv.actors);

			adventure.ctrl.set(adv);
			adventure.ctrl.set('animals', {
				found: [],
				search: '',
				list: ANIMALDATA
			});

			main.initAdventure();
			break;
	}
});

menu.call.register('+remove', '_', function(x) {
	if (!confirm('Do you want to remove ' + menu.ctrl.get(x[0] + '.' + x[1] + '.name') + '?'))
		return;

	var list = menu.ctrl.get(x[0]);
	var rmvd = list.splice(parseInt(x[1]), 1);

	if (main.isOnline() && main.isLogged() && typeof rmvd[0].dbid !== 'undefined') {
		switch (x[0]) {
			case 'characters':
				main.deleteCharacter(rmvd[0].dbid);
				break;
			case 'adventures':
				main.deleteAdventure(rmvd[0].dbid);
				break;
		}
	}

	for (var i = 0, ii = list.length; i < ii; i++) {
		list[i].id = i;
	}

	menu.ctrl.set(x[0], list);
});