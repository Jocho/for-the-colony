var character = app.section('character');

// troubles
character.call.register('+trouble+', '_', function(x) {
	var troubleList = 'troubles.' + x[1] + '.list';
	var troubles = character.ctrl.get(troubleList);
	if (x[3] == 'add') {
		if (!characterHelper.isAlive())
			return;
		troubles.push({});
	}
	else
		troubles.pop();
	character.ctrl.set(troubleList, troubles);

	characterHelper.updatePenalties();
	dice.ctrl.set('limit', ANIMALDATA[5].energy - character.ctrl.get('penalties.roll'));
});

// abilities
character.call.register('addAbility', '_', function() {
	abilityHelper.open(character.ctrl.get('abilities'));
});

// save
character.call.register('saveCharacter', '_', function() {
	var confirmSentense = 'Your mouse has no name. It will not be saved. Continue?';
	var ch = character.ctrl.get();

	if (!ch.name.length) {
		if (confirm(confirmSentense))
			main.initMenu();
		return;
	}

	var characters = menu.ctrl.get('characters');
	ch.changed = main.getTimestamp();

	if (ch.id == -1) {
		ch.id = characters.length;
		characters.push(ch);
	}
	else {
		characters[ch.id] = ch;
	}

	main.initMenu();
	menu.ctrl.set('characters', characters);

	if (main.isOnline() && main.isLogged()) {
		main.saveCharacters();
	}
});

class CharacterHelper {
	getNewMouse() {
		return {
			id: -1,
			name: '',
			notes: '',
			division: 0,
			furColor: 0,
			changed: 0,
			synced: 0,
			abilities: [],
			penalties: {
				body: 0,
				mind: 0,
				roll: 0
			},
			troubles: [
				{type: 'hunger', list: []},
				{type: 'wound', list: []},
				{type: 'disease', list: []},
				{type: 'exhaustion', list: []}
			]
		};
	}

	updatePenalties() {
		var penalties = {
			hunger: 0,
			wound: 0,
			disease: 0,
			exhaustion: 0
		};

		var troubles = character.ctrl.get('troubles');
		for (var trouble of troubles) {
			penalties[trouble.type] = trouble.list.length;
		}

		var penaltyMind = penalties.hunger + penalties.disease;
		var penaltyBody = penalties.wound + penalties.disease;
		var penaltyRoll = penalties.exhaustion;
		var overalPenalty = penalties.hunger + penalties.wound + penalties.disease + penalties.exhaustion;

		character.ctrl.set('penalties', {
			body: penaltyBody,
			mind: penaltyMind,
			roll: penaltyRoll
		});

		var toggle = (penaltyBody) ? true : false;
		this.togglePenaltyIcon('body', toggle);

		toggle = (penaltyMind) ? true : false;
		this.togglePenaltyIcon('mind', toggle);

		toggle = (overalPenalty <= ANIMALDATA[5].vitality);
		this.togglePenaltyIcon('faint', toggle);

		toggle = (overalPenalty > ANIMALDATA[5].vitality);
		this.togglePenaltyIcon('death', toggle);

		this.updateHealthbar();
	}

	updateHealthbar() {
		var cur = this.getPenaltySum();
		var max = ANIMALDATA[5].vitality;
		var html = document.querySelector('.healthbar__bar');

		html.style.height = (100 - (cur * 100) / (max + 1)) + '%';

		if (cur == max) {
			html.classList.add('healthbar__bar--to-faint');
		}
		else {
			html.classList.remove('healthbar__bar--to-faint');
		}
	}

	updateAbilityTypes() {
		var abilities = character.ctrl.get('abilities');
		var htmls = document.querySelectorAll('.abilities__list .abilities__item');

		for (var i = 0, ii = abilities.length; i < ii; i++) {
			if (parseInt(abilities[i].active))
				htmls[i].classList.add('abilities__item--active');
			else
				htmls[i].classList.remove('abilities__item--active');
		}
	}

	togglePenaltyIcon(type, toggle) {
		var html = document.querySelector('.penalty--' + type);
		if (toggle)
			html.classList.remove('hidden');
		else
			html.classList.add('hidden');
	}

	getPenaltySum() {
		var penaltySum = 0;
		var troubles = character.ctrl.get('troubles');

		for (var trouble of troubles) {
			penaltySum += trouble.list.length;
		}

		return penaltySum;
	}

	isAlive() {
		return this.getPenaltySum() <= ANIMALDATA[5].vitality;
	}
}

var characterHelper = new CharacterHelper;