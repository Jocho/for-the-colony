String.prototype.capitalize = function(lower) {
    return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

var adventure = app.section('adventure');
var advScenes = app.section('scenes');
var advActors = app.section('actors');

// scenes
advScenes.call.register('add', '_', function() {
	list = advScenes.ctrl.get('list');
	list.push(adventureHelper.getNewScene());

	for (var i = 0, ii = list.length; i < ii; i++)
		list[i].id = i;

	advScenes.ctrl.set('list', list);
	advScenes.ctrl.set('id', list.length - 1);
});

advScenes.call.register('remove', '_', function() {
	list = advScenes.ctrl.get('list');
	var id = parseInt(advScenes.ctrl.get('id'));
	list.splice(id, 1);

	if (!list.length)
		list.push(adventureHelper.getNewScene());

	for (var i = 0, ii = list.length; i < ii; i++)
		list[i].id = i;

	if (id == list.length)
		advScenes.ctrl.set('id', id - 1, false);

	advScenes.ctrl.set('list', list);
	adventureHelper.initScene(advScenes.ctrl.get('id'));
});

advScenes.call.register('tab', '_', function() {
	var tab = parseInt(advScenes.ctrl.get('tab'));
	tab = (tab +1 < SCENETABS.length) ? (++tab) : 0;
	advScenes.ctrl.set('tab', tab, false);
	adventureHelper.initSceneTab(tab);
});

advScenes.call.register('id', '_', function() {
	adventureHelper.initScene(advScenes.ctrl.get('id'));
});

advScenes.call.register('name', '_', function(x) {
	advScenes.ctrl.set('list.' + advScenes.ctrl.get('id') + '.name', advScenes.ctrl.get('name'));
});

advScenes.call.register('note', '_', function() {
	advScenes.ctrl.set('list.' + advScenes.ctrl.get('id') + '.notes.' + advScenes.ctrl.get('tab'), advScenes.ctrl.get('note'));
});


// search
adventure.call.register('animals.search', '_', function() {
	var search = adventure.ctrl.get('animals.search');
	var findAll = search.match(/^\.{3,}$/);
	if (search.length < 3 && !findAll) {
		adventure.ctrl.set('animals.found', []);
		return;
	}

	search = search.toLowerCase();

	var animals = adventure.ctrl.get('animals.list');
	var found = animals.reduce(function(x, y) {
		var name = y.name.toLowerCase();
		if (name.indexOf(search) > -1 || findAll)
			x.push(y);
		return x;
	}, []);

	adventure.ctrl.set('animals.found', found);
});

adventure.call.register('+addActorAnimal', '_', function(x) {
	var actors = advActors.ctrl.get('actors');
	var animal = adventure.ctrl.get('animals.found.' + x[2]);
	var actor = adventureHelper.getNewActor();
	actor.id = actors.length;
	actor.name = animal.name.capitalize();
	actor.vitality = parseInt(animal.vitality);
	actor.energy = parseInt(animal.energy);

	actors.push(actor);
	advActors.ctrl.set('actors', actors);
	adventureHelper.updatePenalties(actor);
	main.initAutoresizeTextarea();

	// advActors.call.run('+activateActor', '_', ['actors', actor.id]);
});

// actors
advActors.call.register('addActor', '_', function() {
	var actors = advActors.ctrl.get('actors');
	var actor = adventureHelper.getNewActor();
	actor.id = actors.length;
	actors.push(actor);
	advActors.ctrl.set('actors', actors);
	adventureHelper.updatePenalties(actor);
	main.initAutoresizeTextarea();

	advActors.call.run('+activateActor', '_', ['actors', actor.id]);
});

advActors.call.register('+activateActor', '_', function(x) {
	var htmls = document.querySelectorAll('.actor');
	var actor = advActors.ctrl.get('actors.' + x[1]);
	var found = false;

	for (var i = 0, ii = htmls.length; i < ii; i++) {
		if (i == x[1] && !htmls[i].classList.contains('actor--active')) {
			htmls[i].classList.add('actor--active');
			found = true;
		}
		else {
			htmls[i].classList.remove('actor--active');

		}
	}

	if (found)
		adventureHelper.updateDiceLimit(actor);
	else
		adventureHelper.updateDiceLimit();
});

advActors.call.register('+stat+', '_', function(x) {
	var actor = advActors.ctrl.get('actors.' + x[1]);
	var value = actor[x[3]];

	switch (x[4]) {
		case 'raise':
			if (x[3] != 'energy' || value < 6)
				value++;
			break;
		case 'lower':
			value = (value > 2) ? value - 1 : 1;
			break;
	}

	actor[x[3]] = value;

	advActors.ctrl.set('actors.' + x[1], actor);
	adventureHelper.updatePenalties(actor);
});

advActors.call.register('+trouble+', '_', function(x) {
	var actor = advActors.ctrl.get('actors.' + x[1]);
	var troubles = actor.troubles[x[3]].list;

	var lti = troubles.length - 1;
	var vitalityTreshold = adventureHelper.getVitalityTreshold(actor);

	var lastTrouble = (lti > -1) ? troubles[lti] : {val: vitalityTreshold};

	if (x[5] == 'add') {
		if (!adventureHelper.isAlive(actor))
			return;

		if (parseInt(lastTrouble.val) < vitalityTreshold)
			troubles[lti].val++;
		else
			troubles.push({val: 1});
	}
	else {
		if (parseInt(lastTrouble.val) > 1)
			troubles[lti].val--;
		else
			troubles.pop();
	}

	actor.troubles[x[3]].list = troubles;
	advActors.ctrl.set('actors.' + x[1], actor);

	adventureHelper.updatePenalties(actor);
});

advActors.call.register('+division', '_', function(x) {
	adventureHelper.updateActorDivision(advActors.ctrl.get('actors.' + x[1]));
})

advActors.call.register('+removeActor', '_', function(x) {
	adventureHelper.hideActor(x, function(x) {
		actors = advActors.ctrl.get('actors');
		actors.splice(parseInt(x[1]), 1);

		for (var i = 0, ii = actors.length; i < ii; i++) {
			actors[i].id = i;
		}

		advActors.ctrl.set('actors', actors);
	});
});

advActors.call.register('openAbilities', '_', function() {
	abilityHelper.open([]);
});













// save
adventure.call.register('saveAdventure', '_', function() {
	var confirmSentense = 'Your adventure has no title. It will not be saved. Continue?';
	var adv = adventure.ctrl.get();

	if (!adv.name.length) {
		if (confirm(confirmSentense))
			main.initMenu();
		return;
	}

	var adventures = menu.ctrl.get('adventures');
	adv.changed = main.getTimestamp();
	adv.scenes = advScenes.ctrl.get();
	adv.actors = advActors.ctrl.get('actors');

	if (adv.id == -1) {
		adv.id = adventures.length;
		adventures.push(adv);
	}
	else {
		adventures[adv.id] = adv;
	}

	main.initMenu();
	menu.ctrl.set('adventures', adventures);

	if (main.isOnline() && main.isLogged()) {
		main.saveAdventures();
	}
});

class AdventureHelper {
	getNewAdventure() {
		return {
			id: -1,
			name: '',
			changed: 0,
			synced: 0,
			actors: [],
			animals: {
				search: '',
				found: [],
				list: ANIMALDATA
			},
			scenes: {
				id: 0,
				name: '',
				note: '',
				tab: 0,
				tabName: SCENETABS[0],
				list: [this.getNewScene()]
			}
		};
	}

	getNewActor() {
		return {
			id: -1,
			name: '',
			notes: '',
			division: 0,
			vitality: 1,
			energy: 1,
			penalties: {
				mind: 0,
				body: 0,
				roll: 0
			},
			troubles: [
				{type: 'hunger', list: []},
				{type: 'wound', list: []},
				{type: 'disease', list: []},
				{type: 'exhaustion', list: []}
			]
		};
	}

	getNewScene() {
		return {
			id: 0,
			name: TEXTS.newScene,
			notes: ['', '', '', '']
		}
	}

	initScene(id) {
		var scene = advScenes.ctrl.get('list.' + id);
		advScenes.ctrl.set('name', scene.name);
		advScenes.ctrl.set('note', scene.notes[advScenes.ctrl.get('tab')]);
		this.initSceneTab(advScenes.ctrl.get('tab'));
	}

	initSceneTab(id) {
		var scene = advScenes.ctrl.get('list.' + advScenes.ctrl.get('id'));
		advScenes.ctrl.set('note', scene.notes[id]);
		advScenes.ctrl.set('tabName', SCENETABS[id]);
	}

	initPenalties() {
		var actors = advActors.ctrl.get('actors');
		for (var i = 0, ii = actors.length; i < ii; i++) {
			this.updatePenalties(actors[i]);
		}
	}

	getPenaltySum(actor) {
		var penaltySum = 0;
		
		for (var trouble of actor.troubles) {
			penaltySum += trouble.list.reduce(function(x, y) {
				return x + parseInt(y.val);
			}, 0);
		}

		return penaltySum;
	}

	updatePenalties(actor) {
		var penalties = {
			hunger: 0,
			wound: 0,
			disease: 0,
			exhaustion: 0
		};

		var vitalityTreshold = this.getVitalityTreshold(actor);

		for (var trouble of actor.troubles) {
			// penalties[trouble.type] = trouble.list.length;
			penalties[trouble.type] = trouble.list.reduce(function(x, y) {
				return (parseInt(y.val) == vitalityTreshold) ? x + 1 : x;
			}, 0);
		}

		var penaltyMind = penalties.hunger + penalties.disease;
		var penaltyBody = penalties.wound + penalties.disease;
		var penaltyRoll = penalties.exhaustion;
		var overalPenalty = penalties.hunger + penalties.wound + penalties.disease + penalties.exhaustion;

		actor.penalties = {
			body: penaltyBody,
			mind: penaltyMind,
			roll: penaltyRoll
		};

		advActors.ctrl.set('actors.' + actor.id, actor);
		this.updateActorHealth(actor);
		this.updateDiceLimit(actor);
		this.updateActorDivision(actor);
		this.updateActorTroubles(actor);
	}

	updateActorTroubles(actor) {
		var html = document.querySelectorAll('.actor')[actor.id].querySelectorAll('.trouble-block__list');
		
		var vitalityTreshold = this.getVitalityTreshold(actor);

		for (var i = 0, ii = actor.troubles.length; i < ii; i++) {
			var lti = actor.troubles[i].list.length - 1;
			var lastTrouble = actor.troubles[i].list[lti];
			var lastHtml = html[i].querySelectorAll('.trouble__icon')[lti];
			
			if (typeof lastHtml === 'undefined')
				continue;

			lastHtml.style.width = (parseInt(lastTrouble.val) < vitalityTreshold)
				? 100 * parseInt(lastTrouble.val) / vitalityTreshold + '%'
				: '';
		}
	}

	getVitalityTreshold(actor) {
		return Math.ceil(parseInt(actor.vitality) / 6);
	}

	updateActorHealth(actor) {
		var html = document.querySelectorAll('.actor')[actor.id];
		var hpbHtml = html.querySelector('.healthbar__wrapper');
		var hpbbHtml = html.querySelector('.healthbar__bar');
		var faintHtml = html.querySelector('.penalty--faint');
		var deathHtml = html.querySelector('.penalty--death');
		
		var vit = parseInt(actor.vitality);
		var penaltySum = this.getPenaltySum(actor);

		var height = 100 - (penaltySum * 100) / (vit + 1);
		var toFaint = penaltySum == vit;

		if (penaltySum <= vit) {
			faintHtml.classList.remove('hidden');
		}
		else {
			faintHtml.classList.add('hidden');
		}

		if (penaltySum > vit) {
			deathHtml.classList.remove('hidden');
		}
		else {
			deathHtml.classList.add('hidden');
		}

		if (toFaint) {
			hpbbHtml.classList.add('healthbar__bar--to-faint');
		}
		else {
			hpbbHtml.classList.remove('healthbar__bar--to-faint');
		}

		hpbbHtml.style.height = height + '%';

		var icon = html.querySelector('.healthbar__icon');
		if (!parseInt(actor.division)) {
			hpbHtml.classList.add('healthbar--gloss');
			icon.classList.add('hidden');
		}
		else {
			hpbHtml.classList.remove('healthbar--gloss');
			icon.classList.remove('hidden');
		}
	}

	updateDiceLimit(actor) {
		var limit = (typeof actor !== 'undefined')
			? parseInt(actor.energy) - parseInt(actor.penalties.roll)
			: 3;

		dice.ctrl.set('limit', limit);
	}

	updateActorDivision(actor) {
		var html = document.querySelectorAll('.actor')[actor.id];
		var div = html.querySelector('.actor-division');
		div.innerHTML = parseInt(actor.division) ? '(' + DIVISIONS[actor.division] + ')' : '';


		var hbar = html.querySelector('.healthbar__wrapper');
		var icon = hbar.querySelector('.healthbar__icon');
		if (!parseInt(actor.division)) {
			hbar.classList.add('healthbar--gloss');
			icon.classList.add('hidden');
		}
		else {
			hbar.classList.remove('healthbar--gloss');
			icon.classList.remove('hidden');
		}
	}

	isAlive(actor) {
		return this.getPenaltySum(actor) <= parseInt(actor.vitality);
	}

	hideActor(x, callback) {
		var html = document.querySelectorAll('.actor')[x[1]];
		var height = window.getComputedStyle(html);

		html.style.height = height;
		html.classList.add('actor--hide');

		setTimeout(function() {
			callback(x);
		}, 600);
	}
}

var adventureHelper = new AdventureHelper;