var abilityModal = app.section('abilityModal');
abilityModal.ctrl.set('selected', []);
abilityModal.ctrl.set('list', MOUSEABILITIES);

abilityModal.call.register('+select', '_', function(x) {
	if (character.view.isHidden())
		return;
	
	var selected = abilityModal.ctrl.get('selected');

	var selectedIds = selected.reduce(function(x, y) {
		x.push(parseInt(y.id));
		return x;
	}, []);

	var idx = selectedIds.indexOf(MOUSEABILITIES[parseInt(x[1])].id);

	if (idx > -1)
		selected.splice(idx, 1);
	else if (selected.length < MOUSEABILITYAMOUNT)
		selected.push(MOUSEABILITIES[parseInt(x[1])]);
	
	abilityModal.ctrl.set('selected', selected);
	abilityHelper.updateSelected();
});

class AbilityHelper {
	constructor() {
		this.elem = document.querySelector('#ability-list');
		this.closeElem = document.querySelector('#ability-list .close');
		var me = this;
		this.closeElem.addEventListener('click', function() {
			me.close();
		});
	}
	
	open(selected) {
		this.elem.classList.add('active');
		abilityModal.ctrl.set('selected', selected);
		this.updateSelected();
		this.updateAbilityTypes();
	}

	close() {
		character.ctrl.set('abilities', abilityModal.ctrl.get('selected'));
		this.elem.classList.remove('active');
		characterHelper.updateAbilityTypes();
	}

	updateSelected() {
		var htmls = document.querySelectorAll('.ability');
		var selected = abilityModal.ctrl.get('selected');
		var selectedIds = selected.reduce(function(x, y) {
			x.push(parseInt(y.id));
			return x;
		}, []);

		for (var i = 0, ii = MOUSEABILITIES.length; i < ii; i++) {
			if (selectedIds.indexOf(i) > -1)
				htmls[i].classList.add('ability--selected');
			else
				htmls[i].classList.remove('ability--selected');
		}
	}

	updateAbilityTypes() {
		var abilities = abilityModal.ctrl.get('list');
		var htmls = document.querySelectorAll('.list .ability');

		for (var i = 0, ii = abilities.length; i < ii; i++) {
			if (parseInt(abilities[i].active))
				htmls[i].classList.add('abilities__item--active');
			else
				htmls[i].classList.remove('abilities__item--active');
		}
	}
}

var abilityHelper = new AbilityHelper;